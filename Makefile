FFMPEG_URL="https://github.com/FFmpeg/FFmpeg"
FFMPEG_REV=22d1b2498228ee1c39a6d9750b7c0be65c08cc8b

SRC_DIR=$(PWD)/src
BUILD_DIR=$(PWD)/build
PATCH_DIR=$(PWD)/patches
SCRIPTS_DIR=$(PWD)/scripts

get:
	rm -rf $(SRC_DIR)
	git clone $(FFMPEG_URL) $(SRC_DIR)
	[ -n "$(FFMPEG_REV)" ] && cd $(SRC_DIR) && git checkout $(FFMPEG_REV) && cd -

patch:
	cd $(SRC_DIR) && \
	for p in $(PATCH_DIR)/*.patch ; do echo "Applying patch $$p..."; patch -p1 < $$p ; done && \
	cd -

build:
	rm -rf $(BUILD_DIR) && \
	mkdir $(BUILD_DIR) && \
	cd $(BUILD_DIR) && \
	$(SCRIPTS_DIR)/configure.sh $(SRC_DIR) \
	&& make -j4

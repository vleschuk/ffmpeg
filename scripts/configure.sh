#!/bin/sh
[ -z "$1" ] && echo "Source dir is not defined" && exit 1
$1/configure --enable-shared \
             --disable-static \
             --disable-debug \
             --disable-iconv \
             --disable-programs --enable-ffmpeg --enable-ffplay \
             --disable-doc \
             --disable-encoders \
             --disable-protocols --enable-protocol=file \
             --disable-bsfs \
             --disable-indevs \
             --disable-outdevs \
             --disable-network \
             --disable-muxers --enable-muxer=mp5 --enable-muxer=mov \
             --disable-decoders --enable-decoder=aac --enable-decoder=h264 \
             --disable-parsers --enable-parser=aac --enable-parser=h264 \
             --disable-demuxers --enable-demuxer=mov --enable-demuxer=mp5 \
             --disable-filters --enable-filter=fps --enable-filter=aresample --enable-filter=scale
